.. |label| replace:: 1st Vision GmbH - Preisfindungsmodul für Shopware in Verbindung mit dem Office Line Connector
.. |snippet| replace:: FvOLPriceCalculation
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.3.0
.. |maxVersion| replace:: 5.6.4
.. |version| replace:: 3.2.4
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Das Office Line Preisfindungsmodul für Shopware ermöglicht es, große Teile der Office Line Preisfindung im Shop abzubilden. 
Die Datenübertragung erfolgt hierbei über den Connector, sofern OLSI und Connector entsprechend konfiguriert sind.


Folgende Preis-Funktionen können aus der Office Line importiert und im Shop dargestellt werden:

1.	Kundenspezifische Artikelpreise
2.	Kundenspezifische Artikelrabatte
3.	Rabattlisten mit Staffelpreisen und Gültigkeit
4.	Rabattmatrix
5.	Kundenrabattsätze mit Staffelung
6.	Artikel Rabattfähig/nicht Rabattfähig
7.	Preislisten Rabattfähig/nicht Rabattfähig
8.	Export des Rabattsatzes in den Office Line Beleg
9.	Preiseinheiten




Frontend
--------
Im Frontend wird der Preis durch die hinterlegten Rabatt neu berechnet, wenn der Kunde sich eingeloggt hat. Nach Wunsch kann angezeigt werden ob der Listenpreis als Streichpreis angezeigt wird.
Kundenspezifische Artikelnummern können aktiviert werden. Diese werden dann auf der Artikeldetailseite, Warenkorb, Checkout, und Merkzettel ins Template integriert und angezeigt. In das Template der Bestellmail wird es übergeben, aber muss individuell selbst ins Template eingefügt werden.

Backend
-------


Konfiguration
_____________
.. image:: FvOLPriceCalculation1.png
:Listenpreise als Streichpreise anzeigen: Falls aktiv, werden die Streichpreise / Pseudopreise mit den Preislisten-Preisen überschrieben. Dies ermöglicht auch die Übergabe der Rabatte in den Office Line Beleg.
:Kundenspezifischen Preis als Streichpreis anzeigen: Falls aktiv, wird bei Artikeln, die sowohl einen kundenspezifischen Preis als auch einen kundenspezifischen Rabatt haben, der kundenspezifische Preis als Streichpreis / Pseudopreis angezeigt. Ebenso kann dieser dann mit dem kundenspezifischen Rabatt in den Office Line Beleg übergeben werden. 
:Kundenspezifische Artikelnummern anzeigen: Falls aktiv, wird die kundenspezifische Artikelnummer an das Frontend geliefert und mittels Eingreifen ins Template auf der Artikeldetailseite, im Checkout und im Merkzettel angezeigt.
Auch in die Umgebungsvariablen der Bestellmail wird werden die eigenen Artikelnummern geliefert. Nachfolgend ein Beispiel, wie diese Artikelnummern in das Template der Bestellmail integriert werden könnten.
:Listenpreis immer anzeigen: Zeigt den Listenpreis immer an, auch wenn ein kundenspezifischer Preis für den Artikel vorhanden ist.
:Einheitenpreise in Shop: Zeigt die Preiseinheiten im Shop an und berechnet den Preis pro Stück auf 2 Dezimalstellen im Warenkorb wird aber die Berechnung gem. der Preiseinheit weiter berecnet


technische Beschreibung
------------------------
Dieses Plugin funktioniert nur in Verbindung mit dem 1stVision OLSI Connector

Es wird ein neues Freitextfeld bei Artikel/Attribute angelegt.
fv_price_unit

Es kann nun durch den Connector die PreiseinheitVK in das freitextfeld eingetragen werden.

.. image:: FvOLPriceCalculation4.png

Modifizierte Template-Dateien
-----------------------------
:/checkout/items/product.tpl:
:/detail/content/buy_container.tpl:
:/note/item.tpl:



